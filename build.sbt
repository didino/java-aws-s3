name := "play-aws-s3"

organization := "no.didi"

version := "0.0.1-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  "com.amazonaws" % "aws-java-sdk" % "1.10.1"
)

isSnapshot := true

publishMavenStyle := true

publishTo := {
  val nexus = "https://oss.sonatype.org/"
  if (isSnapshot.value)
    Some("snapshots" at nexus + "content/repositories/snapshots")
  else
    Some("releases"  at nexus + "service/local/staging/deploy/maven2")
}

publishArtifact in Test := false

pomIncludeRepository := { _ => false }

pomExtra := (
  <url>http://didi.no</url>
    <licenses>
      <license>
        <name>BSD-style</name>
        <url>http://www.opensource.org/licenses/bsd-license.php</url>
        <distribution>repo</distribution>
      </license>
    </licenses>
    <scm>
      <url>git@github.com:vegaen/play-aws-s3.git</url>
      <connection>scm:git:git@github.com:vegaen/play-aws-s3.git</connection>
    </scm>
    <developers>
      <developer>
        <id>vegaen</id>
        <name>Vegar Engen</name>
        <url>http://didi.no</url>
      </developer>
    </developers>)