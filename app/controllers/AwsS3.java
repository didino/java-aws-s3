package controllers;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.google.inject.Singleton;
import play.Configuration;
import play.Logger;

import java.io.*;
import java.util.function.Supplier;

@Singleton
public class AwsS3 {

    public final String AWS_S3_BUCKET = "aws.s3.bucket";
    public final String AWS_ACCESS_KEY = "aws.access.key";
    public final String AWS_SECRET_KEY = "aws.secret.key";

    private static Supplier<AwsS3> instance = LazilyInstantiate.using(() -> new AwsS3());

    private final String s3Bucket = Configuration.root().getString(AWS_S3_BUCKET);

    private final AmazonS3 amazonS3;

    public static AwsS3 getInstance() {
        return instance.get();
    }

    private AwsS3(){
        Logger.info("Creating object");
        final Configuration conf = Configuration.root();
        final String accessKey = conf.getString(AWS_ACCESS_KEY);
        final String secretKey = conf.getString(AWS_SECRET_KEY);

        //Add accessKey != null && secretKey != null
        AWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
        amazonS3 = new AmazonS3Client(awsCredentials);
        amazonS3.createBucket(s3Bucket);

    }

    public void save(final String fileName, final File file) {
        PutObjectRequest putObjectRequest = new PutObjectRequest(s3Bucket, fileName, file);
        putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);
        //Upload
        amazonS3.putObject(putObjectRequest);
    }

    public void delete(final String filename){
        amazonS3.deleteObject(s3Bucket, filename);
    }

}
