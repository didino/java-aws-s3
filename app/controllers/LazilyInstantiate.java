package controllers;

import com.google.inject.Singleton;

import java.util.function.Supplier;

@Singleton
public class LazilyInstantiate<T> implements Supplier<T> {

    private final Supplier<T> supplier;
    private Supplier<T> current;

    public static <T> LazilyInstantiate<T> using(Supplier<T> supplier){
        return new LazilyInstantiate<>(supplier);
    }

    @Override
    public T get() {
        return current.get();
    }

    private LazilyInstantiate(Supplier<T> supplier) {
        this.supplier = supplier;
        this.current = () -> swapper();
    }

    private synchronized T swapper() {
        if(!Factory.class.isInstance(current)){
            T obj = supplier.get();
            current = new Factory<T>(obj);
        }
        return current.get();
    }
}

class Factory<T> implements Supplier<T> {

    private T obj;

    Factory(T obj){
        this.obj = obj;
    }

    @Override
    public T get() {
        return obj;
    }
}